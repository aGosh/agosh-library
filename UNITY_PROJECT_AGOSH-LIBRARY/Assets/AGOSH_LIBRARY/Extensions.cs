﻿using UnityEngine;
using System;
using System.Collections;

    
public static class ExtensionCoroutine
{

    /// <summary>
    /// Расширение Корутина
    /// </summary>
    /// <typeparam name="T">Тип возвращаемого значения</typeparam>
    /// <param name="obj">MonoBehaviour</param>
    /// <param name="coroutine">Корутин</param>
    /// <returns>Возвращаеет значение типа Т</returns>
    public static Coroutine<T> StartCoroutine<T>( this MonoBehaviour obj, IEnumerator coroutine )
    {
        Coroutine<T> coroutineObject = new Coroutine<T>();
        coroutineObject.coroutine = obj.StartCoroutine( coroutineObject.InternalRoutine( coroutine ) );
        return coroutineObject;
    }

    public static IEnumerator OverTime( this MonoBehaviour obj, float time, Func<float, float> f, Action<float> action )
    {
        float startTime = Time.time;
        while( Time.time - startTime < time )
        {
            float u = f( ( Time.time - startTime ) / time );
            action( u );
            yield return null;
        }
        action( f( 1 ) );
        yield break;
    }

    /// <summary>
    /// Класс нового корутина
    /// </summary>
    /// <typeparam name="T">Тип возвращаемого значения</typeparam>
    public class Coroutine<T>
    {
        private T returnVal;					// Возвращаемое значение
        public T Value
        {
            get
            {
                if( e != null )
                {
                    throw e;
                }
                return returnVal;
            }
        }

        private bool isCancelled = false;		// Требование выйти из цикла
        public void Cancel()
        {
            isCancelled = true;
        }

        public Coroutine coroutine;				// Принятый корутин	
        private Exception e;					// Исключение

        // Внутренний итератор
        public IEnumerator InternalRoutine( IEnumerator coroutine )
        {
            while( true )
            {
                // Проверяю нужно ли закончить выполнение
                if( isCancelled )
                {
                    e = new CoroutineCancelledException();
                    yield break;
                }



                // Двигаю на следующую итерацию и смотрю есть ли ошибки
                try
                {
                    if( !coroutine.MoveNext() )
                    {
                        yield break;
                    }
                }
                catch( Exception e )
                {
                    Debug.Log( "ERROR!:  InternalRoutine		" + e.Message );
                    //Debug.Break();
                    this.e = e;
                    yield break;
                }



                // Возвращаю каждую итерацию...
                object yielded = coroutine.Current;
                if( yielded != null && yielded is T )
                {
                    // ...и если значение выходного типа, то возвращаю его в returnVal и завершаю
                    returnVal = (T) yielded;
                    yield break;
                }
                else
                {
                    yield return coroutine.Current;
                }
            }
        }
    }

    /// <summary>
    /// Исключение выхода из цикла корутина
    /// </summary>
    public class CoroutineCancelledException : System.Exception
    {
        public CoroutineCancelledException()
            : base( "Coroutine was cancelled" )
        {

        }
    }
}
public static class Extensions
{
    /// <summary>
    /// Минимальный угол между направлениями по оси
    /// </summary>
    /// <param name="sourceDir">Первое направление</param>
    /// <param name="targetDir">Второе направление</param>
    /// <param name="axis">По какой оси считается угол</param>
    public static float AngleAroundAxis( this Vector3 sourceDir, Vector3 targetDir, Vector3 axis )
    {
        // Проекция векторов на ось axis
        sourceDir = sourceDir - Vector3.Project( sourceDir, axis );
        targetDir = targetDir - Vector3.Project( targetDir, axis );

        // Определяем угол между векторами
        var angle = Vector3.Angle( sourceDir, targetDir );

        // Угол между целями умножается на -1 если угол между ними больше 90 градусов
        return angle * ( Vector3.Dot( axis, Vector3.Cross( sourceDir, targetDir ) ) < 0 ? -1 : 1 );
    }

    public static float SignedAngle( this Vector3 sourceDir, Vector3 targetDir )
    {
        return Vector3.Angle( sourceDir, targetDir ) * Mathf.Sign( Vector3.Cross( sourceDir, targetDir ).y );
    }
}

namespace UniRx
{

    using Hash = System.Collections.Generic.Dictionary<string, string>;
    using HashEntry = System.Collections.Generic.KeyValuePair<string, string>;

    public static partial class ObservableWWW
    {
        public static IObservable<Texture2D> GetTexture( string url, Hash headers = null, IProgress<float> progress = null )
        {
            return Observable.FromCoroutine<Texture2D>( ( observer, cancellation ) => FetchTexture( new WWW( url, null, ( headers ?? new Hash() ) ), observer, progress, cancellation ) );
        }

        static IEnumerator FetchTexture( WWW www, IObserver<Texture2D> observer, IProgress<float> reportProgress, CancellationToken cancel )
        {
            using( www )
            {
                while( !www.isDone && !cancel.IsCancellationRequested )
                {
                    if( reportProgress != null )
                    {
                        try
                        {
                            reportProgress.Report( www.progress );
                        }
                        catch( Exception ex )
                        {
                            observer.OnError( ex );
                            yield break;
                        }
                    }
                    yield return null;
                }

                if( cancel.IsCancellationRequested ) yield break;

                if( reportProgress != null )
                {
                    try
                    {
                        reportProgress.Report( www.progress );
                    }
                    catch( Exception ex )
                    {
                        observer.OnError( ex );
                        yield break;
                    }
                }

                if( !string.IsNullOrEmpty( www.error ) )
                {
                    observer.OnError( new WWWErrorException( www ) );
                }
                else
                {
                    observer.OnNext( www.texture );
                    observer.OnCompleted();
                }
            }
        }
    }
}

/*
private Transform _transformCache;
	public new Transform transform
	{
		get
		{
			if (!_transformCache) 
				_transformCache = base.transform;
			
			return _transformCache;
		}
	}
*/
