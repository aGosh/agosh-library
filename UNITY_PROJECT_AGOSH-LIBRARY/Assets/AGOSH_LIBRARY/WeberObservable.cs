﻿using UniRx;
using System;
using System.Collections;
using System.Net;
using System.IO;
using UnityEngine;
// compile with: /doc:DocAGOSH.xml
///Библиотека

public static class WeberObservable
{        
    public static IObservable<string> AuthenticationAsObservable( this AGOSH.Weber weber )
    {
        return Observable.FromCoroutine<string>( ( observer, cancellationToken ) => _AuthenticationAsObservable( weber, observer, cancellationToken ) );
    }

    //public IObservable<string> SendComandToLoranAsObservable( string number, string command )
    //{
    //    return Observable.FromCoroutine<string>( ( observer, cancellationToken ) => _SendComandToLoranAsObservable( number, command, observer, cancellationToken ) );
    //}

    public static IObservable<Unit> DownloadStructureAsObservable( this AGOSH.Weber weber, string catalogNumber, string fileName )
    {
        return Observable.FromCoroutine<Unit>( ( observer, cancellationToken ) => _DownloadStructureAsObservable( weber, catalogNumber, fileName, observer, cancellationToken ) );
    }

    public static IObservable<Unit> DownloadFileAsObservable( this AGOSH.Weber weber, string id, string fileName )
    {
        return Observable.FromCoroutine<Unit>( ( observer, cancellationToken ) => _DownloadFileAsObservable( weber, id, fileName, observer, cancellationToken ) );
    }
    

        
    private static IEnumerator _AuthenticationAsObservable( AGOSH.Weber webClient, IObserver<string> observer, CancellationToken cancellationToken )
    {            
        while( !webClient.isRedy ) yield return null;

        try
        {
            Debug.Log( webClient.URL );
            Debug.Log( webClient.Login );
            Debug.Log( webClient.Password );
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL, UriKind.Absolute ), webClient.Method, System.Text.Encoding.UTF8.GetBytes( "name=" + webClient.Login + "&password=" + webClient.Password ) );
        }
        catch( Exception ex )
        {
            observer.OnError( ex );
            yield break;
        }


        while( !webClient.IsDone && !webClient.IsCancelled && !cancellationToken.IsCancellationRequested ) yield return null;

        if( webClient.IsCancelled || cancellationToken.IsCancellationRequested ) yield break;

        if( !string.IsNullOrEmpty( webClient.Error ) ) observer.OnError( new Exception( webClient.Error ) );
        else
        {
            string answer = System.Text.Encoding.UTF8.GetString( webClient.result );
            if( !string.IsNullOrEmpty( answer ) && answer != "200 Login success" )
            {
                observer.OnError( new Exception( answer ) );
            }
            else
            {
                observer.OnNext( answer );
                observer.OnCompleted();
            }
        }        
    }

    //private IEnumerator _SendComandToLoranAsObservable( string number, string command, IObserver<string> observer, CancellationToken cancellationToken )
    //{
    //    if( number == "" || command == "" )
    //        observer.OnError( new Exception( "Bad number or command:		" + number + " " + command ) );//  .OnNext( "not Valid" );

    //    webClient.UploadDataAsync( new Uri( URL + "commands/pushqueue/uid/", UriKind.Absolute ), method, System.Text.Encoding.UTF8.GetBytes( "command=" + command ) );

    //    bool isCancelled = false;
    //    byte[] result = null;

    //    webClient.UploadDataCompleted += ( o, e ) =>
    //    {
    //        isCancelled = e.Cancelled;
    //        result = e.Result;
    //    };

    //    while( !isCancelled && result == null && !cancellationToken.IsCancellationRequested )
    //        yield return null;

    //    if( isCancelled || cancellationToken.IsCancellationRequested )
    //    {
    //        //yield return "was Cancelled";
    //        yield break;
    //    }

    //    observer.OnNext( System.Text.Encoding.UTF8.GetString( result ) );
    //    observer.OnCompleted();
    //}

    private static IEnumerator _DownloadStructureAsObservable( AGOSH.Weber webClient, string catalogNumber, string fileName, IObserver<Unit> observer, CancellationToken cancellationToken )
    {
        while( !webClient.isRedy ) yield return null;

        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL + "commands/structure/category/" + catalogNumber, UriKind.Absolute ), webClient.Method, System.Text.Encoding.UTF8.GetBytes( "" ) );
        }
        catch( Exception ex )
        {
            observer.OnError( ex );
            yield break;
        }


        while( !webClient.IsDone && !webClient.IsCancelled && !cancellationToken.IsCancellationRequested ) yield return null;

        if( webClient.IsCancelled || cancellationToken.IsCancellationRequested ) yield break;

        if( !string.IsNullOrEmpty( webClient.Error ) ) observer.OnError( new Exception( webClient.Error ) );
        else
        {
            try
            {
                File.WriteAllBytes( fileName, webClient.result );
            }
            catch( IOException error )
            {
                observer.OnError( error );
            }
            observer.OnNext( Unit.Default );
            observer.OnCompleted();
        }
    }

    private static IEnumerator _DownloadFileAsObservable( AGOSH.Weber webClient, string id, string fileName, IObserver<Unit> observer, CancellationToken cancellationToken )
    {           
        while( !webClient.isRedy ) yield return null;
                        
        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL + "commands/getdata/id/" + id, UriKind.Absolute ), webClient.Method, System.Text.Encoding.UTF8.GetBytes( "" ) );
        }
        catch( Exception ex )
        {
            observer.OnError( ex );
            yield break;
        }


        while( !webClient.IsDone && !webClient.IsCancelled && !cancellationToken.IsCancellationRequested )  yield return null;

        if( webClient.IsCancelled || cancellationToken.IsCancellationRequested )    yield break;

        if( !string.IsNullOrEmpty( webClient.Error ) )      observer.OnError( new Exception( webClient.Error ) );
        else
        {
            try
            {
                File.WriteAllBytes( fileName, webClient.result );
            }
            catch( IOException error )
            {
                observer.OnError( error );
            }
            observer.OnNext( Unit.Default );
            observer.OnCompleted();
        }
    }
}