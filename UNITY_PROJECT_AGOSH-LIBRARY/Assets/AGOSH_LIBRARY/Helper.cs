﻿using UnityEngine;
using System.IO;
using System;
// compile with: /doc:DocAGOSH.xml
///Библиотека
namespace AGOSH
{
    /// <summary>
    /// Класс со вспомогательными функциями
    /// </summary>
    static public partial class Helper
    {
        /// <summary>
        /// Функция получаения полного пути в папке Resources приложения
        /// </summary>
        /// <param name="localPath">Относительный путь к файлу</param>
        /// <returns>Полный путь</returns>
        public static string GetFullPath( string localPath )
        {
            localPath = localPath.Trim();
            if( localPath.StartsWith( "/" ) || localPath.StartsWith( "\\" ) )
            {
                localPath = localPath.Substring( 1 );
                localPath = GetFullPath() + localPath;
                if( !Directory.Exists( localPath ) )
                    Directory.CreateDirectory( localPath );
            }

            return localPath;
        }
        /// <summary>
        /// Функция получаения полного пути в папке Resources приложения
        /// </summary>
        /// <returns>Полный путь</returns>
        public static string GetFullPath()
        {
            string basePath = Application.dataPath.Trim();
            if( !( basePath.EndsWith( "/" ) || basePath.EndsWith( "\\" ) ) )
            {
                basePath += "/Resources/Data/";

                if( !Directory.Exists( basePath ) )
                    Directory.CreateDirectory( basePath );
            }
            return basePath;
        }
        /// <summary>
        /// Функция получаения полного пути К папке Resources приложения
        /// </summary>
        /// <returns>Полный путь</returns>
        public static string GetResourcesPath()
        {
            string basePath = Application.dataPath.Trim();
            if( !( basePath.EndsWith( "/" ) || basePath.EndsWith( "\\" ) ) )
            {
                basePath += "/Resources/";
            }
            return basePath;
        }
        /// <summary>
        /// Функция копирования файла
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="Destn"></param>
        /// <returns></returns>
        public static bool CopyFiles( string Source, string Destn )
        {
            try
            {
                if( File.Exists( Source ) == true )
                {
                    File.Copy( Source, Destn );
                    return true;
                }
                else
                {
                    Debug.Log( "Source path . does not exist\n" + Source );
                    return false;
                }
            }
            catch( FileNotFoundException exFile )
            {
                Debug.Log( "File Not Found " + exFile.Message );
                return false;
            }
            catch( DirectoryNotFoundException exDir )
            {
                Debug.Log( "Directory Not Found " + exDir.Message );
                return false;
            }
            catch( Exception ex )
            {
                Debug.Log( ex.Message );
                return false;
            }
        }
    }
}