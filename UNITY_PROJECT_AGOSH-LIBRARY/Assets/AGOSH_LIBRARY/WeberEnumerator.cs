﻿using System.Collections;
using System.IO;
using System;
using System.Text;

    public static class WeberEnumerator
    {
    public static IEnumerator Authentication( this AGOSH.Weber webClient, Action<string, Exception> result )
    {
        while( !webClient.isRedy )
        {
            result( null, null );
            yield return null;
        }


        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL, UriKind.Absolute ), webClient.Method, Encoding.UTF8.GetBytes( "name=" + webClient.Login + "&password=" + webClient.Password ) );
        }
        catch( Exception ex )
        {
            result( null, ex );
            yield break;
            //throw ex;
        }


        while( !webClient.IsDone && !webClient.IsCancelled )
        {
            result( null, null );
            yield return null;
        }


        if( webClient.IsCancelled )
        {
            result( null, new Exception( "was cancelled" ) );
            yield break;            
        }


        if( !string.IsNullOrEmpty( webClient.Error ) )
        {
            result( null, new Exception( webClient.Error ) );
            yield break;
            //throw new Exception( webClient.Error );
        }
        else
        {
            string answer = Encoding.UTF8.GetString( webClient.result );
            if( !string.IsNullOrEmpty( answer ) && answer != "200 Login success" )
            {
                result( null, new Exception( answer ) );
                yield break;
                //throw new Exception( answer );
            }
            else
            {
                result( answer, null );
            }
        }
    }
    public static IEnumerator Authentication( this AGOSH.Weber webClient )
    {
        while( !webClient.isRedy )            yield return null;


        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL, UriKind.Absolute ), webClient.Method, Encoding.UTF8.GetBytes( "name=" + webClient.Login + "&password=" + webClient.Password ) );
        }
        catch( Exception ex )
        {
            throw ex;
        }


        while( !webClient.IsDone && !webClient.IsCancelled )    yield return null;


        if( webClient.IsCancelled )     yield break;


        if( !string.IsNullOrEmpty( webClient.Error ) )
        {
            throw new Exception( webClient.Error );
        }
        else
        {
            string answer = Encoding.UTF8.GetString( webClient.result );
            if( !string.IsNullOrEmpty( answer ) && answer != "200 Login success" )
            {
                throw new Exception( answer );
            }
            else
            {
                yield return answer;
            }
        }
    }

    //public IEnumerator SendComandToLoran( string number, string command )
    //{
    //    if( number == "" || command == "" )
    //        yield return "Bad number or command:		" + number + " " + command; //  .OnNext( "not Valid" );

    //    webClient.UploadDataAsync( new Uri( URL + "commands/pushqueue/uid/", UriKind.Absolute ), method, Encoding.UTF8.GetBytes( "command=" + command ) );

    //    bool isCancelled = false;
    //    byte[] result = null;

    //    webClient.UploadDataCompleted += ( o, e ) =>
    //    {
    //        isCancelled = e.Cancelled;
    //        result = e.Result;
    //    };

    //    while( !isCancelled && result == null )
    //        yield return null;

    //    if( isCancelled )
    //    {
    //        yield return "was Cancelled";
    //    }

    //    yield return System.Text.Encoding.UTF8.GetString( result );
    //}

    public static IEnumerator DownloadStructure( this AGOSH.Weber webClient, string catalogNumber, string fileName, Action<bool, Exception> result )
    {
        while( !webClient.isRedy )
        {
            result( false, null );
            yield return null;
        }


        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL + "commands/structure/category/" + catalogNumber, UriKind.Absolute ), webClient.Method, Encoding.UTF8.GetBytes( "" ) );
        }
        catch( Exception ex )
        {
            result( false, ex );
            yield break;
        }


        while( !webClient.IsDone && !webClient.IsCancelled )
        {
            result( false, null );
            yield return null;
        }


        if( webClient.IsCancelled )
        {
            result( false, new Exception( "was cancelled" ) );
            yield break;
        }


        if( !string.IsNullOrEmpty( webClient.Error ) )
        {
            result( false, new Exception( webClient.Error ) );
            yield break;
        }
        else
        {
            try
            {
                File.WriteAllBytes( fileName, webClient.result );
                result( true, null );
            }
            catch( IOException error )
            {
                result( false, error );
            }            
        }
    }

    public static IEnumerator DownloadFile( this AGOSH.Weber webClient, string id, string fileName, Action<bool, Exception> result )
    {
        while( !webClient.isRedy )
        {
            result( false, null );
            yield return null;
        }


        try
        {
            webClient.isRedy = false;
            webClient.UploadDataAsync( new Uri( webClient.URL + "commands/getdata/id/" + id, UriKind.Absolute ), webClient.Method, Encoding.UTF8.GetBytes( "" ) );
        }
        catch( Exception ex )
        {
            result( false, ex );
            yield break;
        }


        while( !webClient.IsDone && !webClient.IsCancelled )
        {
            result( false, null );
            yield return null;
        }


        if( webClient.IsCancelled )
        {
            result( false, new Exception( "was cancelled" ) );
            yield break;
        }


        if( !string.IsNullOrEmpty( webClient.Error ) )
        {
            result( false, new Exception( webClient.Error ) );
            yield break;
        }
        else
        {
            try
            {
                File.WriteAllBytes( fileName, webClient.result );
                result( true, null );
            }
            catch( IOException error )
            {
                result( false, error );
            }
        }
    }
}