﻿using System.Xml.Serialization;
using System.IO;
// compile with: /doc:DocAGOSH.xml
///Библиотека
namespace AGOSH
{
    /// <summary>
    /// Класс для сериализации десериализации xml
    /// </summary>
    static public partial class Serializer
    {
        /// <summary>
        /// Сериализация в файл
        /// </summary>
        /// <typeparam name="T">Тип, который надо сериализовать</typeparam>
        /// <param name="serializebleClass">>экземпляр типа Т, который надо сериализовать</param>
        /// <param name="path">Путь до файла .xml</param>
        public static void Serialize<T>( T serializebleClass, string path )
        {
            var serializer = new XmlSerializer( typeof( T ) );

            using( var file = new StreamWriter( path, false ) )
            {
                serializer.Serialize( file, serializebleClass );
            }
        }
        /// <summary>
        /// Сериализация в строку
        /// </summary>
        /// <typeparam name="T">Тип, который надо сериализовать</typeparam>
        /// <param name="serializebleClass">экземпляр типа Т, который надо сериализовать</param>
        /// <returns>строка с разметкой xml</returns>
        public static string SerializeToText<T>( T serializebleClass )
        {
            var serializer = new XmlSerializer( typeof( T ) );

            using( StringWriter textWriter = new StringWriter() )
            {
                serializer.Serialize( textWriter, serializebleClass );
                return textWriter.ToString();
            }
        }
        /// <summary>
        /// Десериализация из файла
        /// <MonsterCollection>
 	    ///     <Monsters>
 	    ///         <Monster name="a">
 	    ///             <Health>5</Health>
 	    ///	        </Monster>
 	    ///	        <Monster name="b">
 	    ///		        <Health>3</Health>
 	    ///	        </Monster>
 	    ///     </Monsters>
        ///</MonsterCollection>
        ///
        /// public class Monster
        ///{ 
        ///   [XmlAttribute("name")]
        ///   public string Name; 
        ///   public int Health;
        ///}
        ///
        ///[XmlRoot("MonsterCollection")]
        ///public class MonsterContainer
        ///{
        ///    [XmlArray( "Monsters" )]
        ///    [XmlArrayItem( "Monster" )]
        ///    public List<Monster> Monsters = new List<Monster>();
        ///}
        ///
        /// 
        /// </summary>
        /// <typeparam name="T">Тип в который надо Десириализовать</typeparam>
        /// <param name="path">Путь до файла .xml</param>
        /// <returns>Экземпляр тип в который надо Десириализовать</returns>
        public static T Deserialize<T>( string path )
        {
            var serializer = new XmlSerializer( typeof( T ) );

            try
            {
                using( var stream = new FileStream( path, FileMode.Open ) )
                {
                    return (T) serializer.Deserialize( stream );
                }
            }
            catch( System.Exception )
            {
                return default(T);
            }
        }
        /// <summary>
        /// Десериализация из строки
        /// </summary>
        /// <typeparam name="T">Тип в который надо Десериализовать</typeparam>
        /// <param name="path">Путь до файла .xml</param>
        /// <returns>Экземпляр тип в который надо Десериализовать</returns>
        public static T DeserializeFromText<T>( string text )
        {
            var serializer = new XmlSerializer( typeof( T ) );
            return (T) serializer.Deserialize( new StringReader( text ) );
        }
    }
}