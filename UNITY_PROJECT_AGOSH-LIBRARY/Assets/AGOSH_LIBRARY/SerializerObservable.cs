﻿using UniRx;
// compile with: /doc:DocAGOSH.xml
///Библиотека
namespace AGOSH
{
    /// <summary>
    /// Класс для сериализации десериализации xml
    /// </summary>
    static public partial class Serializer
    {
        /// <summary>
        /// Сериализация в файл
        /// </summary>
        /// <typeparam name="T">Тип, который надо сериализовать</typeparam>
        /// <param name="serializebleClass">>экземпляр типа Т, который надо сериализовать</param>
        /// <param name="path">Путь до файла .xml</param>
        /// <returns>Поставщик типа UniRx.Unit</returns>
        public static IObservable<Unit> SerializeAsObservable<T>( T serializebleClass, string path )
        {
            return Observable.Create<Unit>( observer =>
            {
                Serialize<T>( serializebleClass, path );

                observer.OnNext( Unit.Default );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }
        /// <summary>
        /// Сериализация в строку
        /// </summary>
        /// <typeparam name="T">Тип, который надо сериализовать</typeparam>
        /// <param name="serializebleClass">экземпляр типа Т, который надо сериализовать</param>
        /// <returns>Поставщик типа string</returns>
        public static IObservable<string> SerializeToTextAsObservable<T>( T serializebleClass )
        {
            return Observable.Create<string>( observer =>
            {
                observer.OnNext( SerializeToText<T>( serializebleClass ) );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }

        /// <summary>
        /// Десериализация из файла
        /// 
        /// <?xml version="1.0" encoding="utf-8"?>
        /// <AccessData>
        ///     <url>http://sges.imtsoft.ru/</url>
        ///     <login>admin</login>
        ///     <pass>test</pass>
        ///     <catalog>9</catalog>
        /// </AccessData>
        /// 
        ///[System.Serializable]
        ///[XmlRoot( "AccessData" )]
        ///public class MyClass
        ///{
        ///    [XmlElement( "url" )]
        ///    public string _url;
        ///    [XmlElement( "login" )]
        ///    public string _login;
        ///    [XmlElement( "pass" )]
        ///    public string _pass;
        ///    [XmlElement( "catalog" )]
        ///    public string _catalog;
        ///}
        ///
        /// </summary>
        /// <typeparam name="T">Тип в который надо Десириализовать</typeparam>
        /// <param name="path">Путь до файла .xml</param>
        /// <returns>Поставщик типа Т</returns>
        public static IObservable<T> DeserializeAsObservable<T>( string path )
        {
            return Observable.Create<T>( observer =>
            {
                observer.OnNext( Deserialize<T>( path ) );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }
        /// <summary>
        /// Десериализация из строки
        /// </summary>
        /// <typeparam name="T">Тип в который надо Десириализовать</typeparam>
        /// <param name="text">строка с разметкой xml</param>
        /// <returns>Поставщик типа Т</returns>
        public static IObservable<T> DeserializeFromTextAsObservable<T>( string text )
        {
            return Observable.Create<T>( observer =>
            {
                observer.OnNext( DeserializeFromText<T>( text ) );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }
    }
}