﻿using UniRx;
using UnityEngine;
using System.Collections;
using System.IO;
// compile with: /doc:DocAGOSH.xml
///Библиотека
namespace AGOSH
{
    /// <summary>
    /// Класс со вспомогательными функциями
    /// </summary>
    static public partial class Helper
    {
        /// <summary>
        /// Функция получаения полного пути в папке Resources приложения
        /// </summary>
        /// <param name="localPath">Относительный путь к файлу</param>
        /// <returns>Полный путь</returns>
        public static IObservable<string> GetFullPathAsObservable<T>( string localPath )
        {
            return Observable.Create<string>( observer =>
            {
                observer.OnNext( GetFullPath( localPath ) );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }        
        /// <summary>
        /// Функция получаения полного пути в папке Resources приложения
        /// </summary>
        /// <returns>Полный путь</returns>
        public static IObservable<string> GetFullPathAsObservable<T>()
        {
            return Observable.Create<string>( observer =>
            {
                observer.OnNext( GetFullPath() );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }        
        /// <summary>
        /// Функция получаения полного пути К папке Resources приложения
        /// </summary>
        /// <returns>Полный путь</returns>
        public static IObservable<string> GetResourcesPathObservable<T>()
        {
            return Observable.Create<string>( observer =>
            {
                observer.OnNext( GetResourcesPath() );
                observer.OnCompleted();
                return Disposable.Empty;
            } );
        }
        /// <summary>
        /// Функция снятия скриншота
        /// </summary>
        /// <param name="localNetPatch">адрес компьютера в сети, папка, и т.д.</param>
        /// <returns></returns>
        public static IObservable<Unit> TakeScreenShotAsObservable( string folderPath, string localNetPatch )
        {
            return Observable.FromCoroutine<Unit>( ( observer, cancellationToken ) => TakeScreenShot( folderPath, localNetPatch, observer, cancellationToken ) );
        }
        private static IEnumerator TakeScreenShot( string folderPath, string localNetPatch, IObserver<Unit> observer, CancellationToken cancellationToken )
        {
            string fileName = string.Format( "Screenshot {0}.png", System.DateTime.Now.ToString( "yyyy-MM-dd HH-mm-ss" ) );

            if( !Directory.Exists( folderPath ) )
                Directory.CreateDirectory( folderPath );

            // Capture and store the screenshot
            Application.CaptureScreenshot( folderPath + fileName );

            while( !File.Exists( folderPath + fileName ) && !cancellationToken.IsCancellationRequested ) yield return null;

            if( cancellationToken.IsCancellationRequested )
            {
                yield break;
            }
            //if( localNetPatch != "" )
            {
                CopyFiles( folderPath + fileName, localNetPatch + fileName );
            }

            observer.OnNext( Unit.Default );
            observer.OnCompleted();
        }
    }
}