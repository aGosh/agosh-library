﻿using System;
using System.Net;
// compile with: /doc:DocAGOSH.xml
///Библиотека
namespace AGOSH
{
    public partial class Weber : WebClient
    {
        //////////////////////////////
        #region /// Public properties/fields
        public bool isRedy;

        public bool IsDone
        {
            get
            {
                var temp = isDone;
                isDone = false;
                return temp;
            }
        }

        public bool IsCancelled
        {
            get
            {
                var temp = isCancelled;
                isCancelled = false;
                return temp;
            }
        }

        public string Error
        {
            get
            {
                var temp = error;
                error = "";
                return temp;
            }
        }

        public byte[] result = null;


        public string URL { get { return url; } }
        public string Method { get { return method; } }
        public string Login { get { return login; } }
        public string Password { get { return password; } }

        #endregion
        //////////////////////////////


        //////////////////////////////
        #region /// Private properties/fields
        private string url = "";
        private string login = "";
        private string password = "";
        private int timeout = 40000000;
        private string method = "POST";
        private string headers ="X-Requested-With:XMLHttpRequest";
        private string contentType = "application/x-www-form-urlencoded";

        private bool isDone;
        private bool isCancelled;
        private string error;
        #endregion
        //////////////////////////////
        

        //////////////////////////////
        #region /// Public functions
        public static WeberBuilder Create()
        {
            return new WeberBuilder( new Weber() );
        }
        #endregion
        //////////////////////////////


        //////////////////////////////
        #region /// Private functions
        private Weber()
        {
            result = null;
            isDone = false;
            isRedy = true;
            error = "";
        }

        protected override WebRequest GetWebRequest( Uri address )
        {
            var request= base.GetWebRequest( address ) as HttpWebRequest;
            if( request == null )
                return base.GetWebRequest( address );

            request.CookieContainer = new CookieContainer();
            if( WeberBuilder.cookie.Count > 0 )
                request.CookieContainer.Add( WeberBuilder.cookie );
            request.Timeout = timeout;

            return request;
        }

        protected override WebResponse GetWebResponse( WebRequest request )
        {
            HttpWebResponse httpWebResponse = (HttpWebResponse) base.GetWebResponse( request );

            if( httpWebResponse.Cookies != null )
                WeberBuilder.cookie.Add( httpWebResponse.Cookies );

            return httpWebResponse;
        }

        protected override void OnUploadDataCompleted( UploadDataCompletedEventArgs args )
        {
            result = args.Result;
            error = args.Error != null ? args.Error.ToString() : "";
            isCancelled = args.Cancelled;
            isDone = true;
            isRedy = true;

            base.OnUploadDataCompleted( args );
        }
        #endregion
        //////////////////////////////


        public class WeberBuilder
        {
            public static CookieCollection cookie = new CookieCollection();

            private static string url = "";
            private static string login = "";
            private static string password = "";
            private static int timeout = 40000;
            private static string method = "POST";
            private static string headers ="X-Requested-With:XMLHttpRequest";
            private static string contentType = "application/x-www-form-urlencoded";


            private readonly Weber weber;

            internal WeberBuilder( Weber newWeber )
            {
                weber = newWeber;
            }

            public WeberBuilder Url( string url )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Url is empty" );

                WeberBuilder.url = url;
                return this;
            }
            public WeberBuilder Login( string login )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Login is empty" );

                WeberBuilder.login = login;
                return this;
            }
            public WeberBuilder Password( string password )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Password is empty" );

                WeberBuilder.password = password;
                return this;
            }
            public WeberBuilder Timeout( int timeout )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Timeout is empty" );

                WeberBuilder.timeout = timeout;
                return this;
            }
            public WeberBuilder Method( string method )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Method is empty" );

                WeberBuilder.method = method;
                return this;
            }
            public WeberBuilder Headers( string headers )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "Headers is empty" );

                WeberBuilder.headers = headers;
                return this;
            }
            public WeberBuilder ContentType( string contentType )
            {
                if( string.IsNullOrEmpty( url ) ) throw new Exception( "ContentType is empty" );

                WeberBuilder.contentType = contentType;
                return this;
            }

            public Weber Build()
            {
                weber.url = url;
                weber.login = login;
                weber.password = password;
                weber.timeout = timeout;
                weber.method = method;
                weber.headers = headers;
                weber.contentType = contentType;

                weber.Headers.Add( "Content-Type", weber.contentType );
                weber.Headers.Add( weber.headers );
                return weber;
            }
        }
    }
}