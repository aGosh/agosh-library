﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;

public class Test
{
    [Serializable]
    [XmlRoot( ElementName = "category" )]
    public class TestClass
    {
        [XmlAttribute( "name" )]
        public string Name;
        
        [XmlElement("category")]
        public List<QizUnitClass> QizUnit = new List<QizUnitClass>();
    }

    [Serializable]
    public class QizUnitClass
    {
        [XmlAttribute( "name" )]
        public string Name;

        [XmlAttribute( "id" )]
        public string Id;
        
        [XmlElement( "category" )]
        public List<QuestionClass> Questions = new List<QuestionClass>();
    }

    [Serializable]
    public class QuestionClass
    {
        [XmlAttribute( "name" )]
        public string Name;
        [XmlAttribute( "id" )]
        public string Id;        

        [XmlElement( "data" )]
        public DataClass Data;
    }

    [Serializable]
    public class DataClass
    {
        [XmlElement( "image" )]
        public ImageClass Image;
        [XmlElement( "text" )]
        public List<TextClass> Texts = new List<TextClass>();
    }

    [Serializable]
    public class ImageClass
    {
        [XmlAttribute( "name" )]
        public string Name;
        [XmlAttribute( "id" )]
        public string Id;        
    }

    [Serializable]
    public class TextClass
    {
        [XmlAttribute( "name" )]
        public string Name;
        [XmlAttribute( "id" )]
        public string Id;        

        [XmlText]
        public string Text;
    }
}