﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIOnDeselect : Selectable, IDeselectHandler
{
    public bool selectOnStart = true;
    public UnityEvent onDeselect;

    IEnumerator Start()
    {
        while( EventSystem.current == null )
        {
            yield return null;
        }
        if( selectOnStart )
            Select();
    }

    public override void OnDeselect( BaseEventData eventData )
    {
        onDeselect.Invoke();
    }

    protected override void OnValidate()
    {

    }
}